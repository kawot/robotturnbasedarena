﻿using UnityEngine;

public class MoveCard : Card
{
    [SerializeField] float distance = 1f;
    [SerializeField] float speed = 1f;

    private Transform robot;
    private Vector3 startPosition;
    private Vector3 endPosition;

    public override void DoAction(Transform robot)
    {
        this.robot = robot;
        startPosition = robot.position;
        endPosition = robot.position + robot.up * distance;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (robot == null) return;

        var dir = endPosition - robot.position;
        robot.position += dir.normalized * speed * Time.deltaTime;
        if (Vector3.SqrMagnitude(robot.position - startPosition) > distance * distance)
        {
            robot = null;
            OnActionComplete();
        }
    }
}
