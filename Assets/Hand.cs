﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    private static Hand instance;

    [SerializeField] int programSize = 2;
    [SerializeField] int handSize = 5;
    [SerializeField] GameObject emptySlotPrefab;
    [SerializeField] List<Card> deck;
    [SerializeField] Transform placeForDeck;
    [SerializeField] Transform placeForDiscard;
    [SerializeField] Transform placeForProgram;

    private Transform[] slots;
    private Card[] hand;
    private List<Card> discard = new List<Card>();
    private List<Card> program = new List<Card>();

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        if (deck.Count < handSize) Debug.LogError("Deck size is less than hand size");
        if (handSize < programSize) Debug.LogError("Hand size is less than program size");
        SetGUI();
        CreateDeck();
        Shuffle();
        Refill();
    }

    public static void NewTurn()
    {
        instance.Refill();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Refill()
    {
        for (int i = 0; i < handSize; i++)
        {           
            if (hand[i] == null)
            {
                hand[i] = Draw();
                hand[i].transform.SetParent(slots[i]);
                hand[i].transform.localPosition = Vector3.zero;
            }
        }
    }

    public static void SetCommand(Card card)
    {
        if (card.transform.parent != instance.placeForProgram)
        {
            instance.AddCommand(card);
        }
        else
        {
            instance.RemoveCommand(card);
        }
    }

    public static void Discard(Card card)
    {
        card.transform.SetParent(instance.placeForDiscard);
        instance.discard.Add(card);
    }

    private void SetGUI()
    {
        slots = new Transform[handSize];
        hand = new Card[handSize];
        for (int i = 0; i < handSize; i++)
        {
            slots[i] = Instantiate(emptySlotPrefab, transform).transform;
        }
    }

    private void CreateDeck()
    {
        for (int i = 0; i < deck.Count; i++)
        {
            deck[i] = Instantiate(deck[i], placeForDeck).GetComponent<Card>();
        }
    }

    private Card Draw()
    {
        if (deck.Count == 0) Shuffle();
        var cardDrawn = deck[0];
        deck.Remove(deck[0]);
        return cardDrawn;
    }

    private void Shuffle()
    {
        while (deck.Count > 0)
        {
            discard.Add(deck[0]);
            deck.Remove(deck[0]);
        }
        while (discard.Count > 0)
        {
            var rnd = UnityEngine.Random.Range(0, discard.Count);
            deck.Add(discard[rnd]);
            discard[rnd].transform.SetParent(placeForDeck);
            discard.Remove(discard[rnd]);
        }
    }

    private void AddCommand(Card card)
    {
        for (int i = 0; i < handSize; i++)
        {
            if (hand[i] == card)
            {
                hand[i] = null;
                break;
            }
        }
        card.transform.SetParent(placeForProgram);
        card.transform.localPosition = Vector3.zero;
        program.Add(card);
        if (program.Count == programSize) SendProgram();
    }

    private void RemoveCommand(Card card)
    {
        for (int i = 0; i < handSize; i++)
        {
            if (hand[i] == null)
            {
                hand[i] = card;
                card.transform.SetParent(slots[i]);
                card.transform.localPosition = Vector3.zero;
                break;
            }
        }
        program.Remove(card);
    }

    private void SendProgram()
    {
        Gameplay.instance.RunProgram(program);
        while (program.Count > 0)
        {
            discard.Add(program[0]);
            program[0].transform.SetParent(placeForDiscard);
            program.Remove(program[0]);
        }
    }

}
