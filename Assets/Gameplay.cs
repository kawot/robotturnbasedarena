﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    public static Gameplay instance;
    public static bool programIsRunning;

    [SerializeField] Transform robot;

    private List<Card> program = new List<Card>();

    private void Start()
    {
        instance = this;
    }

    public void RunProgram(List<Card> program)
    {
        
        programIsRunning = true;
        this.program = new List<Card>(program);
        RunNextAction();
    }

    private void RunNextAction()
    {
        Debug.Log(program.Count);
        if (program.Count > 0)
        {
            program[0].OnActionComplete += FinishAction;
            program[0].DoAction(robot);
        }
        else
        {
            programIsRunning = false;
            Hand.NewTurn();
            Debug.Log("NEXT TURN");
        }
    }

    private void FinishAction()
    {
        program[0].OnActionComplete -= FinishAction;
        program.Remove(program[0]);
        RunNextAction();
    }

}