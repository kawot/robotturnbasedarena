﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Card : MonoBehaviour
{
    public abstract void DoAction(Transform robot);

    public delegate void ActionComplete();
    public ActionComplete OnActionComplete;

    public void Select()
    {
        if (Gameplay.programIsRunning) return;
        Hand.SetCommand(this);
    }

    public void Discard()
    {
        Hand.Discard(this);
    }
}
